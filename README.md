*version: 3.4.2*

<img src="pics/banner.png" align=center height=300px>

### 📕 Description
[Polybar](https://github.com/polybar/polybar) is a status bars commonly used in window managers.<br>
Is easy to learn and use, is highly customizable, well documented and well compatible with your scripts.<br>
In this repository you can find the themes what i made for my setups.

<br>
<br>

## 🐙 Installation
clone the repository into ~/.config/polybar<br>
then execute in your autostart file:<br>
polybar -c ~/.config/polybar/"theme"/config.ini main & <br>
or execute the _launch.sh _file that's inside every theme folder

```bash
	$ git clone https://gitlab.com/yowls/pbar ~/.config/polybar
```

<br>
<br>

## 🐾 Themes Preview
+ Minla (_soon_)
+ [Blueg](#Blueg)
+ [Fruit](#Fruit)

<br>
<br>

### Blueg
`Full Format`
![polybar](pics/blueg.png)<br>

`Modules`
+ **Modules-left**: Memory - Workspaces
+ **Modules-center**: Time/Date
+ **Modules-right**: Wireless - Volume - Battery - Power-menu

`Details`
+ **Icon font**: Monofur Nerd font
+ **Text font**: Monofur Nerd font
+ **Scripts used**: None
+ **Programs used**: None special?

<br>
<br>

### Fruit
`Full Format`
![polybar](pics/fruit.png)<br>

`Modules`
+ **Modules-left**: brightness - Volume - Cpu - Battery - Time - Date - Weather - Network
+ **Modules-center**: none
+ **Modules-right**: Workspaces - Power menu

`Details`
+ **Icon font**: [Icomoon]
+ **Text font**: Iosevka Nerd Font
+ **Scripts used**: [weather]
+ **Programs used**: none special
